# TSPY analysis on a set of mammals

This Git repository contains a few scripts that have been used during an M2 Bioinformatics internship, which aim was to unravel the **evolutionary trajectories** and the **selective pressures** affecting **TSPY** ampliconic gene family.


# Scripts

Scripts found in this repository allow to download data from the NCBI (*data_download.sh*), to retrieve homologous sequence to a certain reference (*tblastn_search.sh*), to predict gene models (*exonerate_command_line.sh*) and to perform phylogenetic analysis (*phyml_command_line.sh*).

## Requirements
In order to run each script, one must have installed the appropriate set of tools, such as the NCBI command line *datasets* as described bellow :

    $ conda create -n ncbi_datasets
    $ conda activate ncbi_datasets
    $ conda install -c conda-forge ncbi-datasets-cli

One can install *exonerate* with the following instruction :

    $ wget --output-document exonerate-2.2.0-x86_64.tar.gz https://ftp.ebi.ac.uk/pub/software/vertebrategenomics/exonerate/exonerate-2.2.0-x86_64.tar.gz

## Future prospects
All the work carried out resulted in a certain amount of data collected, which have sometimes been quite challenging to examine. This didn't leave room for true script development due to the limited time of the internship, but one goal for the future is to build a clean and improved analysis pipeline. 