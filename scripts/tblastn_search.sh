#!/usr/bin/bash
#####
# Author : Naell LENOIR
# Date (création) : 24/04/2024
# Name : tblastn_search.sh
# Making a blast database from downloaded NCBI accessions (via data_download.sh) 
# Running TBLASTN search
#####

TSPY_FAM="TSPX"
DIRNAME="name_of_analysis_directory"

TSPY_QUERY="name_of_file_containing_query"
DIR="$HOME"/"$DIRNAME"
TSPY_DIR="$DIR"/"$TSPY_FAM"
files=("$TSPY_DIR"/*.fna)
declare -i nb_files
nb_files="${#files[@]}"-1

# makeblastdb : generate an indexed database of nucleotide sequences
for file in $(seq 0 $nb_files) ; do
  makeblastdb -in ${files[$file]} -dbtype nucl -out $(basename ${files[$file]} .fna) &
done
wait

# Running tblastn search
for file in ${files[@]} ; do
  DB=$(basename $file .fna)
  tblastn -query "$DIR"/"$TSPY_QUERY" -db $DB > "$TSPY_DIR"/"$TSPY_FAM"_blast_results_"$DB".txt
done
