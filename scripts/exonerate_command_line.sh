#!/usr/bin/bash
#####
# Author : Naell LENOIR
# Date (création) : 29/04/2024
# Name : exonerate_command_line_example.sh
# Running exonerate analysis 
#####

DIR="name_of_working_directory"
QUERY="name_of_query_fasta_file"
TARGET="name_of_target_sequence_file"
qTYPE="query_type"
tTYPE="target_type"
MODEL="model_to_use"
params="parameters_to_use"
OUTPUT="name_of_output_file"

nohup exonerate -q "$DIR"/"$QUERY" -t "$DIR"/"$TARGET" -Q "$qTYPE" -T "$tTYPE" -m "$MODEL" "$params" > "$OUTPUT" &