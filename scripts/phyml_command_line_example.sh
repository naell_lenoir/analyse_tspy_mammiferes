#!/usr/bin/bash
#####
# Author : Naell LENOIR
# Date (création) : 14/02/2024
# Name : phyml_command_line_example.sh
# Running PhyML on a phylip alignment
#####

DIR="name_of_working_directory"
ALIGNMENT="name_of_alignment_file_in_phylip_format"
SEQTYPE="either_aa_or_nt"
SUBSMOD="substitution_model_to_use"
nCPU="number_of_cpu"
OUTPUT="name_of_output_file"
bootstrap="number_of_boostrap_to_use"

nohup mpirun -np "$nCPU" phyml-mpi -i "$DIR"/"$ALIGNMENT" -d "$SEQTYPE" -m "SUBSMOD" -b "$bootstrap" > "$OUTPUT" & disown 