#!/usr/bin/bash
#####
# Author : Naell LENOIR
# Date (création) : 24/04/2024
# Name : data_download.sh
# Downloading data from NCBI using a TSV file containing organism name, accession (RefSeq or Genbank), and chromosome(s)
# Renaming file after unzipping
#####

TSPY_FAM="TSPX"
DIRNAME="name_of_analysis_directory"

DIR="$HOME"/"$DIRNAME"
TSPY_DIR="$DIR"/"$TSPY_FAM"
INFOS="$DIR"/organism_accession_chromosome_"$TSPY_FAM".tsv
ZIPDIR="$TSPY_DIR"/zipped
LOGS="$TSPY_DIR"/logs

mkdir "$TSPY_DIR"
mkdir "$ZIPDIR"
mkdir "$LOGS"
cd "$TSPY_DIR"

while IFS=$' \t' read org acc chr; do
  CHRFILE="$org"_chr"$chr"
  nohup datasets download genome accession "$acc" --chromosomes "$chr" --filename "$CHRFILE".zip > "$LOGS"/DL_"$org"_chr"$chr".out &
  wait
  ORGDIR="$TSPY_DIR"/"$org"_"$chr"
  unzip -d "$ORGDIR" "$CHRFILE".zip ncbi_dataset/data/G*/chr"$chr".fna
  mv "$ORGDIR"/ncbi_dataset/data/G*/chr"$chr".fna "$TSPY_DIR"/"$CHRFILE".fna
  mv "$CHRFILE".zip "$ZIPDIR"
  rm -rf "$ORGDIR"
done < "$INFOS"